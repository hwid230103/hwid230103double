# Hw id 230103
Light
1. Развернуть Виртуальную машину с Linux. Можно использовать любые хостинги и способы в точ числе Vagrant.
2. Установить docker и запустить minio из стандартного образа.
3. Освоить docker-secret, создать для minio access key, secret key внутри docker secret.
4. Создать четыре виртуальные машины(как в пункте 1), освоить работу docker swarm и включить все машины в общий кластер docker swarm.
5. Развернуть minio через docker stack в кластере docker swarm.
6. Освоить dockerfile инструкцию healthcheck и добавить проверку в stack.

Normal:
1. Написать Ansible Playbook для развёртывания docker на 4 нодах.
2. Написать Ansible Playbook для развёртывания кластера docker swarm на 4 нодах, вынести в отдельную роль атомарные операции, типа установки Docker и создание Swarm.
3. Создать отдельную Ansible роль для развёртывания minio через swarm(ansible-role-minio-swarm).
4.Учесть в ролях создание docker-secretsб автоматические ip адреса узлов, вынести все настраиваемые параметры в переменные.
5. Сделать jinja-шаблон для stack-файла, который динамически формируется с учетом количества нод и их настроек.
6. Покрыть роли тестами через molecule и настроить подключение и версионирование через личный galaxy в gitlab.

Hard:

1. Развернуть minio standalone в облачном Kubernetes, на ваш выбор.
2. Развернуть в kubernetes minio-operator.